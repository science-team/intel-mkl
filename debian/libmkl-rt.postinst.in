#! /bin/sh
set -e

# source debconf library
. /usr/share/debconf/confmodule

# priority 1 is even lower than the netlib reference implementation
# This should be safe enough if the package depends on a free alternative.
_mkl_rt_priority=1

update-alternatives --install /usr/lib/@DEB_HOST_MULTIARCH@/libblas.so.3 \
                    libblas.so.3-@DEB_HOST_MULTIARCH@ \
                    /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so \
                    $_mkl_rt_priority

update-alternatives --install /usr/lib/@DEB_HOST_MULTIARCH@/liblapack.so.3 \
                    liblapack.so.3-@DEB_HOST_MULTIARCH@ \
                    /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so \
                    $_mkl_rt_priority

if [ "amd64" = "@DEB_HOST_ARCH@" ]; then

  update-alternatives --install /usr/lib/@DEB_HOST_MULTIARCH@/libblas64.so.3 \
                      libblas64.so.3-@DEB_HOST_MULTIARCH@ \
                      /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so \
                      $_mkl_rt_priority
  
  update-alternatives --install /usr/lib/@DEB_HOST_MULTIARCH@/liblapack64.so.3 \
                      liblapack64.so.3-@DEB_HOST_MULTIARCH@ \
                      /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so \
                      $_mkl_rt_priority

fi

db_get libmkl-rt/use-as-default-blas-lapack
if [ "$RET" = "true" ]; then

  # When user says "yes", we point the symlinks to MKL, and set the
  # alternative in manual mode.
  echo "Setting MKL as default BLAS/LAPACK implementation as requested." 1>&2
  db_get libmkl-rt/exact-so-3-selections

  if (echo "$RET" | grep "libblas.so" 1>/dev/null 2>&1 ); then
  update-alternatives --set libblas.so.3-@DEB_HOST_MULTIARCH@ \
                      /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so
  fi

  if (echo "$RET" | grep "liblapack.so" 1>/dev/null 2>&1); then
  update-alternatives --set liblapack.so.3-@DEB_HOST_MULTIARCH@ \
                      /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so
  fi

  if [ "amd64" = "@DEB_HOST_ARCH@" ]; then

    if (echo "$RET" | grep "libblas64.so" 1>/dev/null 2>&1 ); then
    update-alternatives --set libblas64.so.3-@DEB_HOST_MULTIARCH@ \
                        /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so
    fi

    if (echo "$RET" | grep "liblapack64.so" 1>/dev/null 2>&1); then
    update-alternatives --set liblapack64.so.3-@DEB_HOST_MULTIARCH@ \
                        /usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so
    fi

  fi

else

  # When user says "no", we do nothing. However if the user says "no" when
  # MKL is selected, we should literally unselect it even if it's in manual
  # mode.
  echo "Unselecting MKL if they were previously selected." 1>&2

  realblas=$(update-alternatives --query libblas.so.3-@DEB_HOST_MULTIARCH@ \
             | grep ^Value: | cut -d ' ' -f 2)
  reallapack=$(update-alternatives --query liblapack.so.3-@DEB_HOST_MULTIARCH@ \
               | grep ^Value: | cut -d ' ' -f 2)

  if [ "$realblas" = "/usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so" ]; then
    update-alternatives --auto libblas.so.3-@DEB_HOST_MULTIARCH@
  fi

  if [ "$reallapack" = "/usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so" ]; then
    update-alternatives --auto liblapack.so.3-@DEB_HOST_MULTIARCH@
  fi

  if [ "amd64" = "@DEB_HOST_ARCH@" ]; then

    realblas64=$(update-alternatives --query libblas64.so.3-@DEB_HOST_MULTIARCH@ \
               | grep ^Value: | cut -d ' ' -f 2)
    reallapack64=$(update-alternatives --query liblapack64.so.3-@DEB_HOST_MULTIARCH@ \
                 | grep ^Value: | cut -d ' ' -f 2)

    if [ "$realblas64" = "/usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so" ]; then
      update-alternatives --auto libblas64.so.3-@DEB_HOST_MULTIARCH@
    fi

    if [ "$reallapack64" = "/usr/lib/@DEB_HOST_MULTIARCH@/libmkl_rt.so" ]; then
      update-alternatives --auto liblapack64.so.3-@DEB_HOST_MULTIARCH@
    fi

  fi
  # We are sure that MKL will be unselected by putting these alternatives
  # back into auto mode, because this package depends on free alternatives.

fi

#DEBHELPER#

exit 0
